Onboarding for SRE Resources

## Read/Watch these first

First, make sure you are familiar with overall SRE methodology, read through the Google SRE Book and understand all key terminology

https://landing.google.com/sre/books/

Also read through the SRE workbook

https://vivint.sharepoint.com/sites/SiteReliabilityEngineering/SitePages/Free-e-Book-SRE-workbook.aspx

This is a good video on how Netflix handles Devops

https://www.youtube.com/watch?v=UTKIT6STSVM

Another good one from Liz Fong Jones on SRE

https://www.youtube.com/watch?v=tEylFyxbDLE

## General Vivint Information

This page from our corporate site gives a good general idea of the products and services that Vivint provides.

https://thehub.vivint.com/marketing/info/product-vault/


## Team Info

Useful Links:

Team Sharepoint - https://vivint.sharepoint.com/sites/infra/SitePages/Home.aspx           
Solarwinds - https://solarwinds.vivint.com/Orion/SummaryView.aspx?ViewID=1       
OpsGenie tutorial - https://youtu.be/UZeT0YUoxrE      
Vivint IO - https://vivint.atlassian.net/wiki/spaces/vivintio/overview     
Vivint IO Dev Portal - https://portal-apio.vivint.com/     
F5 - https://10.2.252.21/tmui/login.jsp?msgcode=1&    
Remote Desktop - https://sreadmin.vivint.com/RDWeb/Pages/en-US/Default.aspx     
Release Calendar - https://vivint.atlassian.net/wiki/spaces/Release/calendars     
SSIS/Snowflake - https://vivint.atlassian.net/wiki/spaces/data/pages/796689411/Step+by+step+-+setting+up+SSIS     
Splunk - https://splunk.vivint.com           
PCI Jumphost - Remote Desktop pci-jh01
Sreadmin.com


Host Naming Convention

<lane><lifecycle><OS><hostname>.apex.local

## Tools

Royal TS - https://www.royalapps.com/ts/mac/download
Atlassian (bitbucket/confluence) - email approval from the team
A- account - 


## Lanes On Prem

On Prem

AC = Account Creation

D2H = Direct to Home (StreetGenie and TechGenie)

Fin = Finance

FS = Field Service

Devops = Devops (jenkins, dev tools, etc)

RTL = Retail (stores like BestBuy)

NIS = Inside Sales (inbound calls from customers and visits at vivint.com)

HR = Human Resources

INF = Infrasstructure (our team)

SGS = Monitoring Platform (items alerted - i.e 911 calls for customers)

CORP = Corporate Network (the hub)

CORE = Shared Services

## Lanes Azure Services/Cloud (vivint.io):

Referral

Digital Survey

Outbox

Amigo - https://vivint.sharepoint.com/:w:/r/Security/_layouts/15/Doc.aspx?sourcedoc=%7BFA4BDB46-95F2-48F7-832D-C1573438176E%7D&file=Security%20Requirements_Amigo.docx&action=default&mobileredirect=true&DefaultItemOpen=1